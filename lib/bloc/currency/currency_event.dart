part of 'currency_bloc.dart';

abstract class CurrencyEvent extends Equatable {
  const CurrencyEvent();
}

class GetCurrency extends CurrencyEvent {
  const GetCurrency();

  @override
  List<Object> get props => [];
}