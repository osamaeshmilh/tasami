import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:Tasami/data/model/currency.dart';
import 'package:Tasami/data/repository/Currency_Repository.dart';
import 'package:equatable/equatable.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'currency_event.dart';
part 'currency_state.dart';

class CurrencyBloc extends Bloc<CurrencyEvent, CurrencyState> {
  final CurrencyRepository repository;

  CurrencyBloc(this.repository);

  @override
  CurrencyState get initialState => CurrencyInitial();

  @override
  Stream<CurrencyState> mapEventToState(
    CurrencyEvent event,
  ) async* {
    // TODO: implement mapEventToState
    // Emitting a state from the asynchronous generator
    yield CurrencyLoading();
    // Branching the executed logic by checking the event type
    if (event is GetCurrency) {
      // Emit either Loaded or Error
      try {
        final List<Currency> currencies = await repository.fetchCurrency();
        if (currencies.isEmpty && currencies == null) {
          yield CurrencyEmpty();
        } else {
          yield CurrencyLoaded(currencies);
        }
      } catch(Exception) {
        yield CurrencyError("Couldn't fetch currency. Is the device online?");
      }
    }
  }
}
