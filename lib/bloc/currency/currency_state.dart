part of 'currency_bloc.dart';

abstract class CurrencyState extends Equatable {
  const CurrencyState();
}

class CurrencyInitial extends CurrencyState {
  const CurrencyInitial();
  @override
  List<Object> get props => [];
}

class CurrencyLoading extends CurrencyState {
  const CurrencyLoading();
  @override
  List<Object> get props => [];
}

class CurrencyEmpty extends CurrencyState {
  const CurrencyEmpty();
  @override
  List<Object> get props => [];
}

class CurrencyLoaded extends CurrencyState {
  final List<Currency> currency;

  const CurrencyLoaded(this.currency);
  @override
  List<Object> get props => [currency];
}

class CurrencyError extends CurrencyState {
  final String message;
  const CurrencyError(this.message);
  @override
  List<Object> get props => [message];
}