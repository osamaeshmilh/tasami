part of 'exchange_bloc.dart';

abstract class ExchangeEvent extends Equatable {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ExchangePressed extends ExchangeEvent {
  final String result;
  ExchangePressed(this.result);
  @override
  // TODO: implement props
  List<Object> get props => [result];
}

class ExchangeInit extends ExchangeEvent {
  @override
  // TODO: implement props
  List<Object> get props => [];
}

class ExchangeDo extends ExchangeEvent {
  // TODO: implement props
  List<Object> get props => [];
}