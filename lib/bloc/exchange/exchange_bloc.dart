import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'exchange_event.dart';
part 'exchange_state.dart';

class ExchangeBloc extends Bloc<ExchangeEvent, ExchangeState> {

  @override
  Stream<ExchangeState> mapEventToState(
    ExchangeEvent event,
  ) async* {
    // TODO: implement mapEventToState
    if(event is ExchangePressed){
      yield ExchangeLoaded(event.result);
    }else if(event is ExchangeInit){
      yield ExchangeInitial();
    }else if(event is ExchangeDo){
      yield ExchangeDoing();
    }
  }

  @override
  // TODO: implement initialState
  ExchangeState get initialState => ExchangeInitial();
}