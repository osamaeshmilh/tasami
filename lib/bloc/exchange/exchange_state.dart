part of 'exchange_bloc.dart';

abstract class ExchangeState extends Equatable {}

class ExchangeInitial extends ExchangeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}


class ExchangeLoaded extends ExchangeState {

  final String num;

  ExchangeLoaded(this.num);

  @override
  // TODO: implement props
  List<Object> get props => [num];
}


class ExchangeDoing extends ExchangeState {
  @override
  // TODO: implement props
  List<Object> get props => [];
}