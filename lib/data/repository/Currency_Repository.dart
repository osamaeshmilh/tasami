import 'dart:convert';
import 'package:Tasami/data/model/currency.dart';
import 'package:dio/dio.dart' as DIO;

abstract class CurrencyRepository {
  Future<List<Currency>> fetchCurrency();
  // Future<Currency> fetchDetailedCurrency(String cityName);
}

class ApiCurrencyRepository implements CurrencyRepository {
  DIO.Dio dio = DIO.Dio();

  @override
  Future<List<Currency>> fetchCurrency() async{
      DIO.Response response = await dio.get("https://currency.tasaminews.ly/api/currencies");

      var currencies = List<Currency>();
      if (response.statusCode == 200) {
        try{
            var CurrenciesJson = response.data;
            for (var currencyJson in CurrenciesJson) {
              currencies.add(Currency.fromJson(currencyJson));
            }
            return currencies;
        } catch(_){
            print('Error from Response.\n');
            print(_);
        }
      } else {
        return null;
      }
  }

  // @override
  // Future<Currency> fetchDetailedCurrency(String cityName) {
  //   return Future.delayed(
  //     Duration(seconds: 1),
  //     () {
  //       return Currency(
  //         cityName: cityName,
  //         temperatureCelsius: cachedTempCelsius,
  //         temperatureFarenheit: cachedTempCelsius * 1.8 + 32,
  //       );
  //     },
  //   );
  // }
}

class NetworkError extends Error {}