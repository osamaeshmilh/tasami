class Currency {
  int id;
  String gBP;
  String uSD;
  String eUR;
  String tRY;
  String jOD;
  String tND;
  String eGP;
  String tRYTransfer;
  String aEDTransfer;
  String checkJumhouria;
  String familyCard;
  String gold;
  String silver;
  String createdAt;

  Currency(
      {this.id,
        this.gBP,
        this.uSD,
        this.eUR,
        this.tRY,
        this.jOD,
        this.tND,
        this.eGP,
        this.tRYTransfer,
        this.aEDTransfer,
        this.checkJumhouria,
        this.familyCard,
        this.gold,
        this.silver,
        this.createdAt});

  Currency.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    gBP = json['GBP'];
    uSD = json['USD'];
    eUR = json['EUR'];
    tRY = json['TRY'];
    jOD = json['JOD'];
    tND = json['TND'];
    eGP = json['EGP'];
    tRYTransfer = json['TRYTransfer'];
    aEDTransfer = json['AEDTransfer'];
    checkJumhouria = json['CheckJumhouria'];
    familyCard = json['familyCard'];
    gold = json['Gold'];
    silver = json['silver'];
    createdAt = json['created_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['GBP'] = this.gBP;
    data['USD'] = this.uSD;
    data['EUR'] = this.eUR;
    data['TRY'] = this.tRY;
    data['JOD'] = this.jOD;
    data['TND'] = this.tND;
    data['EGP'] = this.eGP;
    data['TRYTransfer'] = this.tRYTransfer;
    data['AEDTransfer'] = this.aEDTransfer;
    data['CheckJumhouria'] = this.checkJumhouria;
    data['familyCard'] = this.familyCard;
    data['Gold'] = this.gold;
    data['silver'] = this.silver;
    data['created_at'] = this.createdAt;
    return data;
  }
}