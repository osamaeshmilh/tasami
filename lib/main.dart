//import 'package:Tasami/CurrencyDetail.dart';
import 'package:Tasami/bloc/currency/currency_bloc.dart';
import 'package:Tasami/bloc/exchange/exchange_bloc.dart';
import 'package:Tasami/calculator_Screen.dart';
import 'package:Tasami/data/model/currency.dart';
import 'package:Tasami/data/repository/Currency_Repository.dart';
import 'package:Tasami/exchane_page.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_shimmer/flutter_shimmer.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:url_launcher/url_launcher.dart';

import 'firebase_notification_handler.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CurrencyBloc>(
          create: (context) => CurrencyBloc(ApiCurrencyRepository()),
        ),
        BlocProvider<ExchangeBloc>(
          create: (context) => ExchangeBloc(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          Locale("ar"), // OR Locale('ar', 'AE') OR Other RTL locales
        ],
        locale: Locale("ar"),
        title: 'Tasami News',
        theme: ThemeData(
          primarySwatch: MaterialColor(0xFF141E30, {
            50: Color.fromRGBO(20, 30, 48, .1),
            100: Color.fromRGBO(20, 30, 48, .2),
            200: Color.fromRGBO(20, 30, 48, .3),
            300: Color.fromRGBO(20, 30, 48, .4),
            400: Color.fromRGBO(20, 30, 48, .5),
            500: Color.fromRGBO(20, 30, 48, .6),
            600: Color.fromRGBO(20, 30, 48, .7),
            700: Color.fromRGBO(20, 30, 48, .8),
            800: Color.fromRGBO(20, 30, 48, .9),
            900: Color.fromRGBO(20, 30, 48, 1),
          }),
        ),
        home: MyHomePage(title: 'Tasami News'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  CurrencyBloc currencyBloc;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    currencyBloc = BlocProvider.of<CurrencyBloc>(context);
    currencyBloc.add(GetCurrency());
    _firebaseMessaging.configure(
      onMessage: (message) async {
        currencyBloc.add(GetCurrency());
      },
      onResume: (message) async {
        currencyBloc.add(GetCurrency());
      },
    );

    timeago.setLocaleMessages('ar', timeago.ArMessages());
  }

  @override
  Widget build(BuildContext context) {
    new FirebaseNotifications(context).setUpFirebase();
    return Scaffold(body: Container(
      child: BlocBuilder<CurrencyBloc, CurrencyState>(
        builder: (context, state) {
          if (state is CurrencyInitial) {
            return buildInitialInput();
          } else if (state is CurrencyLoading) {
            return buildLoading();
          } else if (state is CurrencyEmpty) {
            return buildEmpty();
          } else if (state is CurrencyLoaded) {
            return buildColumnWithData(context, state.currency);
          } else if (state is CurrencyError) {
            return buildInitialInput();
          }
        },
      ),
    ));
  }

  Widget buildInitialInput() {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 180),
        child: MyLoadingAppBar(),
      ),
      backgroundColor: Color(0xFF141E30),
      body: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.only(left: 4.0, right: 4.0, top: 4),
          child: Center(
            child: ListView.builder(
              itemCount: 10,
              itemBuilder: (context, index) => ProfileShimmer(
                isPurplishMode: true,
//                 isDarkMode: true,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildLoading() {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 180),
        child: MyLoadingAppBar(),
      ),
      backgroundColor: Color(0xFF141E30),
      body: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.only(left: 4.0, right: 4.0, top: 4),
          child: Center(
            child: ListView.builder(
              itemCount: 10,
              itemBuilder: (context, index) => ProfileShimmer(
                isPurplishMode: true,
//                 isDarkMode: true,
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildEmpty() {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 180),
        child: MyLoadingAppBar(),
      ),
      backgroundColor: Color(0xFF141E30),
      body: Container(
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.only(left: 4.0, right: 4.0, top: 4),
          child: Center(
            child: Text('لا يوجد بيانات للعملات الرجاء اعادة المحاوله'),
          ),
        ),
      ),
    );
  }

  List<double> data_USD;
  List<double> data_EUR;
  List<double> data_GBP;
  List<double> data_TRY;
  List<double> data_JOD;
  List<double> data_TND;
  List<double> data_EGP;
  List<double> data_AED;
  List<double> data_Gold;
  List<double> data_Silver;

  Widget buildColumnWithData(BuildContext context, List<Currency> currency) {
//    this.data_USD = [
////      double.parse(currency[5].uSD),
////      double.parse(currency[4].uSD),
//      double.parse(currency[3].uSD),
//      double.parse(currency[2].uSD),
//      double.parse(currency[1].uSD),
//      double.parse(currency[0].uSD)
//    ];
//    this.data_EUR = [
////      double.parse(currency[5].eUR),
////      double.parse(currency[4].eUR),
////      double.parse(currency[3].eUR),
////      double.parse(currency[2].eUR),
////      double.parse(currency[1].eUR),
////      double.parse(currency[0].eUR)
//    ];
//    this.data_GBP = [
////      double.parse(currency[5].gBP),
////      double.parse(currency[4].gBP),
//      double.parse(currency[3].gBP),
//      double.parse(currency[2].gBP),
//      double.parse(currency[1].gBP),
//      double.parse(currency[0].gBP)
//    ];
//    this.data_TRY = [
////      double.parse(currency[5].tRY),
////      double.parse(currency[4].tRY),
//      double.parse(currency[3].tRY),
//      double.parse(currency[2].tRY),
//      double.parse(currency[1].tRY),
//      double.parse(currency[0].tRY)
//    ];
//    this.data_JOD = [
////      double.parse(currency[5].jOD),
////      double.parse(currency[4].jOD),
//      double.parse(currency[3].jOD),
//      double.parse(currency[2].jOD),
//      double.parse(currency[1].jOD),
//      double.parse(currency[0].jOD)
//    ];
//    this.data_TND = [
////      double.parse(currency[5].tND),
////      double.parse(currency[4].tND),
//      double.parse(currency[3].tND),
//      double.parse(currency[2].tND),
//      double.parse(currency[1].tND),
//      double.parse(currency[0].tND)
//    ];
//    this.data_EGP = [
////      double.parse(currency[5].eGP),
////      double.parse(currency[4].eGP),
//      double.parse(currency[3].eGP),
//      double.parse(currency[2].eGP),
//      double.parse(currency[1].eGP),
//      double.parse(currency[0].eGP)
//    ];
////    this.data_AED = [
//////      double.parse(currency[5].aED),
//////      double.parse(currency[4].aED),
////      double.parse(currency[3].aED),
////      double.parse(currency[2].aED),
////      double.parse(currency[1].aED),
////      double.parse(currency[0].aED)
////    ];
//    this.data_Gold = [
////      double.parse(currency[5].gold),
////      double.parse(currency[4].gold),
//      double.parse(currency[3].gold),
//      double.parse(currency[2].gold),
//      double.parse(currency[1].gold),
//      double.parse(currency[0].gold)
//    ];
//    this.data_Silver = [
////      double.parse(currency[5].silver),
////      double.parse(currency[4].silver),
//      double.parse(currency[3].silver),
//      double.parse(currency[2].silver),
//      double.parse(currency[1].silver),
//      double.parse(currency[0].silver)
//    ];
    final List<List<String>> img_url_list = [
//      [
//        'http://tasaminews.ly/wp-content/uploads/2020/06/ADD.png',
//        'https://www.facebook.com/NCDC.LY/',
//      ],
      [
        'http://tasaminews.ly/wp-content/uploads/2020/06/adver.png',
        'https://www.facebook.com/%D8%AA%D8%B3%D8%A7%D9%85%D9%8A-%D9%86%D9%8A%D9%88%D8%B2-Tasami-News-105315247860219/',
      ],
      [
        'http://tasaminews.ly/wp-content/uploads/2020/06/e-com.png',
        'https://www.elzad.ly/en_US/shop',
      ],
      [
        'http://tasaminews.ly/wp-content/uploads/2020/06/Facebook.png',
        'https://www.facebook.com/%D8%AA%D8%B3%D8%A7%D9%85%D9%8A-%D9%86%D9%8A%D9%88%D8%B2-Tasami-News-105315247860219/',
      ]
    ];

    final List<Widget> imageSliders = img_url_list
        .map((item) => Container(
              child: Container(
                margin: EdgeInsets.all(5.0),
                child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    child: GestureDetector(
                      child: Image.network(
                        item[0],
                        fit: BoxFit.cover,
                        width: 1000,
                      ),
                      onTap: () {
                        click_it(item[1]);
                      },
                    )),
              ),
            ))
        .toList();

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size(double.infinity, 180),
        child: MyFlexiableAppBar(currency[0]),
      ),
      backgroundColor: Color(0xFF141E30),
      body: Stack(
        children: <Widget>[
          RefreshIndicator(
            child: ListView.builder(
//          physics: BouncingScrollPhysics(),
              itemCount: 14,
              itemBuilder: (context, index) {
                return FullCard(currency, index);
              },
            ),
            onRefresh: () {
              currencyBloc.add(GetCurrency());
            },
          ),
          Positioned(
            bottom: 0,
            left: 0,
            child: Container(
              height: MediaQuery.of(context).size.height * .12,
              width: MediaQuery.of(context).size.width,
              color: Colors.white,
              child: CarouselSlider(
                options: CarouselOptions(
//                  height: MediaQuery.of(context).size.height* .21,
                  aspectRatio: 2.0,
                  enlargeCenterPage: false,
                  scrollDirection: Axis.horizontal,
                  autoPlay: true,
                ),
                items: imageSliders,
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget MyFlexiableAppBar(Currency currency) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    final fifteenAgo = DateTime.parse(currency.createdAt);
    final timedago = timeago.format(fifteenAgo, locale: 'ar');
    var date = currency.createdAt.split("T");

    return Container(
      padding: EdgeInsets.only(top: statusBarHeight),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'تسامي نيوز',
                    style: TextStyle(fontSize: 24, color: Colors.white, fontFamily: "Cairo"),
                  ),
                  IconButton(
                    icon: Icon(FontAwesomeIcons.facebook, color: Colors.white),
                    onPressed: () {
                      click_it("https://m.facebook.com/%D8%AA%D8%B3%D8%A7%D9%85%D9%8A-%D9%86%D9%8A%D9%88%D8%B2-Tasami-News-105315247860219/");
                    },
                  )
                ],
              ),
            ),
//              alignment: Alignment.topRight,
          ),
          Container(
            width: 165,
            height: 40,
            child: Center(
              child: OutlineButton(
                textColor: Colors.white,
                highlightColor: Colors.white12,
                borderSide: BorderSide(width: 2),
                disabledBorderColor: Colors.white,
                highlightedBorderColor: Colors.white,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ExchangePage(title: 'التحويلات', currency: currency)));
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'تحويل عملات',
                      style: TextStyle(
                        fontFamily: "Cairo",
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(width: 10),
                    Icon(
                      FontAwesomeIcons.exchangeAlt,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 8.0, left: 8.0),
                      child: Text("اخر تحديث", style: const TextStyle(color: Colors.white, fontFamily: 'Cairo', fontSize: 16.0)),
                    ),
                  ),
//                      SizedBox(height: 40,),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 8.0, right: 8.0),
                      child: Container(
                          child: Row(
                        children: <Widget>[
                          Text(
                            '$timedago',
                            style: const TextStyle(color: Colors.white, fontFamily: 'Cairo', fontSize: 16.0),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            date[0],
                            style: const TextStyle(color: Colors.white, fontFamily: 'Cairo', fontSize: 16.0),
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Container(
                            child: Icon(
                              FontAwesomeIcons.calendarAlt,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      )),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget MyLoadingAppBar() {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    return Container(
      padding: EdgeInsets.only(top: statusBarHeight),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 16.0, right: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    'تسامي نيوز',
                    style: TextStyle(fontSize: 24, color: Colors.white, fontFamily: "Cairo"),
                  ),
                  IconButton(
                    icon: Icon(Icons.refresh, color: Colors.white),
                    onPressed: () {
                      currencyBloc.add(GetCurrency());
                    },
                  )
                ],
              ),
            ),
//              alignment: Alignment.topRight,
          ),
          Container(
            width: 165,
            height: 40,
            child: Center(
              child: OutlineButton(
                textColor: Colors.white,
                highlightColor: Colors.white12,
                borderSide: BorderSide(width: 2),
                disabledBorderColor: Colors.white,
                highlightedBorderColor: Colors.white,
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => Calculator()));
                },
                child: Row(
                  children: <Widget>[
                    Text(
                      'آلـــه حـاسـبـه',
                      style: TextStyle(
                        fontFamily: "Cairo",
                        fontSize: 18,
                      ),
                    ),
                    SizedBox(width: 10),
                    Icon(
                      FontAwesomeIcons.calculator,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            ),
          ),
          Container(
            height: 38,
          ),
        ],
      ),
    );
  }

  Widget SparkLine(var data, int index) {
    return Container(
      child: Sparkline(
        lineWidth: 2,
        pointsMode: PointsMode.last,
        data: data[index],
      ),
    );
  }

// up arrow \u2191
// down arrow \u2193
  Widget Percentage(List<Currency> currency, int index) {
    Color color = Colors.red;
    double total = 0;
    String arrow = "\u2191";
    String value = "0.00";

    switch (index) {
      case 0:
        {
          total = (double.parse(currency[0].uSD) - double.parse(currency[1].uSD)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 1:
        {
          total = (double.parse(currency[0].eUR) - double.parse(currency[1].eUR)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 2:
        {
          total = (double.parse(currency[0].gBP) - double.parse(currency[1].gBP)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 3:
        {
          total = (double.parse(currency[0].tRY) - double.parse(currency[1].tRY)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 4:
        {
          total = (double.parse(currency[0].jOD) - double.parse(currency[1].jOD)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 5:
        {
          total = (double.parse(currency[0].tND) - double.parse(currency[1].tND)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 6:
        {
          total = (double.parse(currency[0].eGP) - double.parse(currency[1].eGP)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 7:
        {
          total = (double.parse(currency[0].tRYTransfer) - double.parse(currency[1].tRYTransfer)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 8:
        {
          total = (double.parse(currency[0].aEDTransfer) - double.parse(currency[1].aEDTransfer)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 9:
        {
          total = (double.parse(currency[0].checkJumhouria) - double.parse(currency[1].checkJumhouria)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 10:
        {
          total = (double.parse(currency[0].familyCard) - double.parse(currency[1].familyCard)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 11:
        {
          total = (double.parse(currency[0].gold) - double.parse(currency[1].gold)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
          break;
        }

      case 12:
        {
          total = (double.parse(currency[0].silver) - double.parse(currency[1].silver)) * -1;
          color = total >= 0 ? Colors.red : Colors.green;
          arrow = total >= 0 ? "\u2193" : "\u2191";
        }
    }

    return Container(
      height: 23,
      color: color,
      padding: const EdgeInsets.all(2),
      child: Center(
        child: Text(
          "$arrow" + "${((double.parse(total.toStringAsFixed(2))).abs())}دل",
          style: TextStyle(
            color: Colors.white,
            fontFamily: 'Poppins',
            fontSize: 12.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
    );
  }

  Widget FullCard(List<Currency> currency, int index) {
    List<String> images = [
      "assets/images/iconfinder_United-States-of-America_298411.png",
      "assets/images/Flag_of_Europe.png",
      "assets/images/iconfinder_United-Kingdom_298478.png",
      "assets/images/iconfinder_Turkey_298423.png",
      "assets/images/iconfinder_Jordan_298464.png",
      "assets/images/iconfinder_Tunisia_298552.png",
      "assets/images/iconfinder_Egypt_298477.png",
      "assets/images/iconfinder_Turkey_298423.png",
      "assets/images/iconfinder_United-Arab-Emirates_298414.png",
      "assets/images/iconfinder_Finance_bank_check_1889186.png",
      "assets/images/pay.png",
      "assets/images/gold.png",
      "assets/images/silver-image.png",
    ];

    List<String> countries = [
      "دولار",
      "يورو",
      "باوند",
      "ليرة التركيه",
      "دينار الاردني",
      "دينار التونسي",
      "جنيه المصري",
      "حواله تركيا",
      "حواله دبي",
      'شيك مصدق',
      'بطاقة ارباب الاسر',
      "ذهب",
      "فضه",
    ];

    List<String> PriceCurrency = [
      currency[0].uSD,
      currency[0].eUR,
      currency[0].gBP,
      currency[0].tRY,
      currency[0].jOD,
      currency[0].tND,
      currency[0].eGP,
      currency[0].tRYTransfer,
      currency[0].aEDTransfer,
      currency[0].checkJumhouria,
      currency[0].familyCard,
      currency[0].gold,
      currency[0].silver
    ];

    SharedPreferences prefs;
    sync() async {
      prefs = await SharedPreferences.getInstance();
      prefs.setString('USD', currency[0].uSD);
      prefs.setString('GBP', currency[0].gBP);
      prefs.setString('TRY', currency[0].tRY);
      prefs.setString('EUR', currency[0].eUR);
      prefs.setString('JOD', currency[0].jOD);
      prefs.setString('TND', currency[0].tND);
      prefs.setString('EGP', currency[0].eGP);
    }

    if (index == 13) {
      return SizedBox(
        height: MediaQuery.of(context).size.height * .12,
      );
    }
    return Container(
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
        child: Card(
          child: InkWell(
//            onTap: () => Navigator.push(
//                context,
//                MaterialPageRoute(
//                    builder: (context) => CurrencyDetail(
//                          currency: currency,
//                        ))),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Image.asset(
                            images[index],
                            width: 30,
                            height: 30,
                          ),
                        ),
                        SizedBox(width: 8),
                        Expanded(
                          flex: 2,
                          child: Text(
                            countries[index],
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              color: Color(0xFF141E30),
                              fontFamily: "Cairo",
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(width: 4),
                          Expanded(
                            child: Percentage(currency, index),
                          ),
                          SizedBox(
                            width: 3,
                          ),
                          Expanded(
                            flex: 2,
                            child: Row(
                              children: <Widget>[
                                Container(
                                  child: Text(PriceCurrency[index], style: TextStyle(color: const Color(0xFF141E30), fontFamily: 'Cairo', fontWeight: FontWeight.w800, fontSize: 21.0)),
                                ),
                                SizedBox(
                                  width: 4,
                                ),
                                Container(
                                  child: Text("د.ل", style: TextStyle(color: const Color(0xFF141E30), fontFamily: 'Cairo', fontWeight: FontWeight.w800, fontSize: 10)),
                                ),
                              ],
                              mainAxisAlignment: MainAxisAlignment.center,
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void customLaunch(command) async {
    if (await canLaunch(command)) {
      await launch(command);
    } else {
      print(' could not launch $command');
    }
  }

  void click_it(String item) {
    customLaunch(item);
  }
}
