
import 'package:Tasami/bloc/exchange/exchange_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:gradient_widgets/gradient_widgets.dart';
import 'package:math_expressions/math_expressions.dart';

class Calculator extends StatefulWidget {
  @override
  _CalculatorState createState() => _CalculatorState();
}

class _CalculatorState extends State<Calculator> {

  String strInput = "";
  final textControllerInput = TextEditingController();
  final textControllerResult = TextEditingController();

  //Text Controllers for taking inputs and showing results
  @override
  void initState() {
    super.initState();
    // Start listening to changes
    textControllerInput.addListener((){});
    textControllerResult.addListener((){});
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    textControllerInput.dispose();
    textControllerResult.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Card(
            child: Padding(
              padding: EdgeInsets.only(right: 18 , left: 18 , top: 8 , bottom: 8),
              child: Row(
                verticalDirection: VerticalDirection.up,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        child: Icon(FontAwesomeIcons.calculator),
                      ),
                      SizedBox(width: 18),
                      Text('الدينار الليبي' , style: TextStyle(
                        color: Color(0xFF141E30),
                        fontFamily: "Cairo",
                        fontSize: 18,
                      ),),
                    ],
                  ),
                  Row(
                    children: [
                      Text(textControllerInput.text,style: TextStyle(
                        color: Color(0xFF141E30),
                        fontFamily: "Cairo",
                        fontSize: 18,
                      ),),
                      Text("  د.ل",style: TextStyle(
                        color: Color(0xFF141E30),
                        fontFamily: "Cairo",
                        fontSize: 18,
                      ),)
                    ],
                  )
                ],
              ),
            ),
          ),
          Expanded(child: Container(),),
           Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child:  TextField(
                decoration:  InputDecoration.collapsed(
                    hintText: "0",
                    hintStyle: TextStyle(
                      fontSize: 30,
                      fontFamily: 'RobotoMono',
                    )),
                style: TextStyle(
                  fontSize: 30,
                  fontFamily: 'RobotoMono',
                ),
                textAlign: TextAlign.right,
                controller: textControllerInput,
                onTap: () =>
                    FocusScope.of(context).requestFocus( FocusNode()),
              )),
           Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: TextField(
                decoration:  InputDecoration.collapsed(
                    hintText: "الناتج",
                    // fillColor: Colors.deepPurpleAccent,
                    hintStyle: TextStyle(fontFamily: 'RobotoMono')),
                textInputAction: TextInputAction.none,
                keyboardType: TextInputType.number,
                style: TextStyle(
                    fontSize: 32,
                    fontFamily: 'RobotoMono',
                    fontWeight: FontWeight.bold
                  // color: Colors.deepPurpleAccent
                ),
                textAlign: TextAlign.right,
                controller: textControllerResult,
                onTap: () {
                },)),
          SizedBox( height: 20.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              btnAC('AC',const Color(0xFFF5F7F9),),
              btnClear(),
              btn('%',const Color(0xFFF5F7F9),),
              btn('/',const Color(0xFFF5F7F9),),],),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              btn('9', Colors.white),
              btn('8', Colors.white),
              btn('7', Colors.white),
              btn('*',const Color(0xFFF5F7F9),),],),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              btn('6', Colors.white),
              btn('5', Colors.white),
              btn('4', Colors.white),
              btn('-',const Color(0xFFF5F7F9),),],),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              btn('3', Colors.white),
              btn('2', Colors.white),
              btn('1', Colors.white),
              btn('+', const Color(0xFFF5F7F9)),],),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              btn('0', Colors.white),
              btn('.', Colors.white),
              btnEqual('='),],),
          SizedBox(height: 10.0,)],),
    );
  }


  Widget btn(btntext, Color btnColor) {
    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      child: FlatButton(
        child: Text(
          btntext,
          style: TextStyle(
              fontSize: 28.0, color: Colors.black, fontFamily: 'RobotoMono'),
        ),
        onPressed: () {
          setState(() {
            textControllerInput.text = textControllerInput.text + btntext;
          });
        },
        color: btnColor,
        padding: EdgeInsets.all(18.0),
        splashColor: Colors.black,
        shape: CircleBorder(),
      ),
    );
  }

  Widget btnClear() {
    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      child: FlatButton(
        child: Icon(Icons.backspace, size: 35, color: Colors.blueGrey),
        onPressed: () {
          textControllerInput.text = (textControllerInput.text.length > 0)
              ? (textControllerInput.text
              .substring(0, textControllerInput.text.length - 1))
              : "";
        },
        color: const Color(0xFFF5F7F9),
        padding: EdgeInsets.all(18.0),
        splashColor: Colors.black,
        shape: CircleBorder(),
      ),
    );
  }
  Widget btnAC(btntext, Color btnColor) {
    return Container(
      padding: EdgeInsets.only(bottom: 10.0),
      child: FlatButton(
        child: Text(
          btntext,
          style: TextStyle(
              fontSize: 28.0, color: Colors.black, fontFamily: 'RobotoMono'),
        ),
        onPressed: () {
          setState(() {
            textControllerInput.text = "";
            textControllerResult.text = "";
          });
        },
        color: btnColor,
        padding: EdgeInsets.all(18.0),
        splashColor: Colors.black,
        shape: CircleBorder(),
      ),
    );
  }

  Widget btnEqual(btnText) {
    return GradientButton(
      child: Text(
        btnText,
        style: TextStyle(fontSize: 35.0),
      ),
      increaseWidthBy: 40.0,
      increaseHeightBy: 10.0,
      callback: () {
        //Calculate everything here
        // Parse expression:
        Parser p =  Parser();
        // Bind variables:
        ContextModel cm =  ContextModel();
        Expression exp = p.parse(textControllerInput.text);
        setState(() {
          textControllerResult.text =
              exp.evaluate(EvaluationType.REAL, cm).toString();
          BlocProvider.of<ExchangeBloc>(context).add(ExchangePressed(textControllerResult.text));
        });
      },
      gradient: Gradients.serve,
    );
  }
}
