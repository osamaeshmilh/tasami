import 'package:Tasami/bloc/exchange/exchange_bloc.dart';
import 'package:Tasami/calc.dart';
import 'package:Tasami/data/model/currency.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ExchangePage extends StatefulWidget {
  ExchangePage({Key key, this.title, this.currency}) : super(key: key);

  final Currency currency;
  final String title;

  @override
  _ExchangePageState createState() => _ExchangePageState();
}

class _ExchangePageState extends State<ExchangePage> {
  ExchangeBloc bloc;

  var currency;
  var name;

  @override
  void initState() {
    bloc = BlocProvider.of<ExchangeBloc>(context);
    bloc.add(ExchangeInit());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title,
          style: TextStyle(fontFamily: "Cairo"),
        ),
      ),
      body: BlocBuilder<ExchangeBloc, ExchangeState>(
        builder: (context, state) {
          if (state is ExchangeInitial) {
            return _buildInitail("1");
          } else if (state is ExchangeLoaded) {
            return _buildLoaded(state.num);
          } else if (state is ExchangeDoing) {
            return Calculator();
          }
        },
      ),
    );
  }

  String _value = "";

  _buildInitail(String num) {
    return Center(
      child: Currency_Converter(num),
    );
  }

  _buildLoaded(String num) {
    return Center(child: Currency_Converter(num));
  }

  Widget Card_Currency(String num) {
    return GestureDetector(
      onTap: () {
        bloc.add(ExchangeDo());
      },
      child: Card(
        child: Padding(
          padding: EdgeInsets.only(right: 18, left: 18, top: 8, bottom: 8),
          child: Row(
            verticalDirection: VerticalDirection.up,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    child: Icon(FontAwesomeIcons.calculator),
                  ),
                  SizedBox(width: 18),
                  Text(
                    "الدينار الليبي",
                    style: TextStyle(
                      color: Color(0xFF141E30),
                      fontFamily: "Cairo",
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
              Flexible(
                child: RichText(
                  overflow: TextOverflow.ellipsis,
                  strutStyle: StrutStyle(fontSize: 12.0),
                  text: TextSpan(
                      style: TextStyle(
                        color: Color(0xFF141E30),
                        fontFamily: "Cairo",
                        fontSize: 18,
                      ),
                      text: num + " د.ل"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget Currency(List<String> currency, int index, String num) {
    double currency_1 = 0;

    switch (index) {
      case 1:
        print("USD : " + widget.currency.uSD);
        currency_1 = double.parse(widget.currency.uSD);
        break;
      case 2:
        print("EUR : " + widget.currency.eUR);
        currency_1 = double.parse(widget.currency.eUR);
        break;
      case 3:
        print("GBP : " + widget.currency.gBP);
        currency_1 = double.parse(widget.currency.gBP);
        break;
      case 4:
        print("TRY : " + widget.currency.tRY);
        currency_1 = double.parse(widget.currency.tRY);
        break;
      case 5:
        print("JOD : " + widget.currency.jOD);
        currency_1 = double.parse(widget.currency.jOD);
        break;
      case 6:
        print("TND : " + widget.currency.tND);
        currency_1 = double.parse(widget.currency.tND);
        break;
      case 7:
        print("EGP : " + widget.currency.eGP);
        currency_1 = double.parse(widget.currency.eGP);
        break;
    }
    double currency_2 = double.parse(num);
    double total = 0;
//    if(currency_1 > 1){
    total = currency_2 / currency_1;
//    }else{
//      total = currency_2 * currency_1;
//    }

    List<String> images = [
      "",
      "assets/images/iconfinder_United-States-of-America_298411.png",
      "assets/images/Flag_of_Europe.png",
      "assets/images/iconfinder_United-Kingdom_298478.png",
      "assets/images/iconfinder_Turkey_298423.png",
      "assets/images/iconfinder_Jordan_298464.png",
      "assets/images/iconfinder_Tunisia_298552.png",
      "assets/images/iconfinder_Egypt_298477.png",
    ];

    return Card(
      child: Padding(
        padding: EdgeInsets.only(right: 18, left: 18, top: 8, bottom: 8),
        child: Row(
          verticalDirection: VerticalDirection.up,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Image.asset(
                  images[index],
                  width: 30,
                  height: 30,
                ),
                SizedBox(width: 18),
                Text(
                  currency[0],
                  style: TextStyle(
                    color: Color(0xFF141E30),
                    fontFamily: "Cairo",
                    fontSize: 18,
                  ),
                ),
              ],
            ),
            Flexible(
              child: RichText(
                overflow: TextOverflow.ellipsis,
                strutStyle: StrutStyle(fontSize: 12.0),
                text: TextSpan(
                    style: TextStyle(
                      color: Color(0xFF141E30),
                      fontFamily: "Cairo",
                      fontSize: 18,
                    ),
                    text: total.toStringAsFixed(2).toString() +
                        '  ' +
                        currency[2]),
              ),
            )
          ],
        ),
      ),
    );
  }

  final List<List<String>> currencies = [
    ['', ''],
    ['دولار', '5.86', '\$'],
    ['اليورو', '6.55', '\€'],
    ['الباوند', '7.25', '\£'],
    ['ليرة التركية', '0.85', '\₺'],
    ['الدينار الاردني', '8.22', 'د.ا'],
    ['الدينار التونسي', '2.08', 'د.ت'],
    ['الجنيه المصري', '0.37', 'ج.م']
  ];

  Widget Currency_Converter(String num) {
    return ListView.builder(
      itemCount: currencies.length,
      itemBuilder: (context, index) {
        if (index == 0) {
          return Card_Currency(num);
        } else {
          return Currency(currencies[index], index, num);
        }
      });
  }
}
